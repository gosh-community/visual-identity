# GOSH Visual Identity

This repository contains the design files for the GOSH LOGO.

The logos are in SVG form. SVG is an editable, scaleable, open format.

![](IdentityAll_Curves.svg)


## Curves version

Logos with text also haves a "curves" version of the file. Here the text has been converted to a drawing. Use this if you are not sure if the correct fonts are installed.

## Other formats

You can convert the logo to other formats using [Inkscape](https://inkscape.org/). We will soon write a script to convert them.
